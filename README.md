# FYP

A project to automate the deployment of a high availablility kubernetes cluster with persistent storage, monitoring and internal loadbalancing

## Contents
- [Prerequsites](#prerequsites)
- [Quick Start](#quick-start)
- [Config](#config)
- [Deploy test application](#deploy-test-application)

## Prerequsites

* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html "Ansible") - Configure VMs and Install Kubernetes
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads "VirtualBox") - Hypervisor
* [Vagrant](https://www.vagrantup.com/downloads.html "Vagrant") - Provision VMs


## Quick Start

This will create and provision a single master, 3 worker node kubernetes cluster with persistent storage, monitoring and internal loadbalancer

1. Create the virtual machines
```console
fyp  >  vagrant up
```
2. Disable Anisble ssh host key checking
```console
fyp  >  export ANSIBLE_HOST_KEY_CHECKING=False
```
3. Run Ansible playbook to provision the platform
```console
fyp  > ansible-playbook k8s-deploy.yml -i .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory
```
4. The master node can be accessed through ssh
```console
fyp  > vagrant ssh k8s-master-1
```

### Additional services endpoints
- Default Ingress - http://192.168.44.240
- Prometheus - http://192.168.44.241:9090
- Grafana - http://192.168.44.242:3000
- Alert-Manager - http://192.168.44.243:9093


## Config
```yaml
---
image: "centos/7"
subnet: "192.168.44"
master:
  number: 1
  cpu: 2
  mem: 4096
worker:
  number: 3
  cpu: 2
  mem: 2048
  disk_size_gb: 30
metallb: true
persistent_storage: true
monitoring: true
helm: true
```
- image - do not change (CentOS 7 is only currently available)
- subnet - can be changed to match current IP space
- master-number - specifing 1 will provision a non-HA cluster, specificing more than 1 will provision a HA cluster with 2 loadbalancer nodes
- cpu - the number of CPUs for the VM
- mem - the amount of memory for the VM (megabytes)
- disk_size_gb - the additional size for worker nodes (gigabytes)
- additional services - set to true or false if they are required or not

## Deploy test application

1. ssh to k8s-master-1 node

```console
(ansible) fyp  >  vagrant ssh k8s-master-1
```

2. Clone Hipster shop repository

```console
[vagrant@k8s-master-1 ~]$ git clone https://github.com/GoogleCloudPlatform/microservices-demo.git
```

3. Create deployment of Hipster shop

```console
[vagrant@k8s-master-1 ~]$ kubectl apply -f microservices-demo/release/kubernetes-manifests.yaml
```

4. Check the LoadBalancer IP for the frontend-external service

```console
[vagrant@k8s-master-1 ~]$ kubectl get services
```

7. The hipster shop can now be accessed on a web browser using the LoadBalancer IP

    e.g. http://192.168.44.243