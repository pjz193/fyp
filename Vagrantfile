# -*- mode: ruby -*-
# # vi: set ft=ruby :

# Specify minimum Vagrant version and Vagrant API version
VAGRANTFILE_API_VERSION = "2"

# Require YAML module
require 'yaml'

# Read YAML file with box details
servers = YAML.load_file('config.yaml')

# Create boxes
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.provision "deploy", type: "ansible" do |ansible|
    ansible.playbook = "add_to_inventory.yml"
    if servers['master']['number'] > 1
      ansible.groups = {
        "lb" => ["lb-1", "lb-2"],
        "primary_master" => ["k8s-master-1"],
        "master" => ["k8s-master-[1:#{servers['master']['number']}]"],
        "node" => ["k8s-node-[1:#{servers['worker']['number']}]"]
      }
    else
      ansible.groups = {
        "primary_master" => ["k8s-master-1"],
        "master" => ["k8s-master-[1:#{servers['master']['number']}]"],
        "node" => ["k8s-node-[1:#{servers['worker']['number']}]"]
      }
    end
  end

  if servers['master']['number'] > 1
    (1..2).each do |i_lb|
      config.vm.define "lb-#{i_lb}" do |loadbalancer|
        loadbalancer.vm.box = servers['image']
        loadbalancer.vm.network "private_network", ip: servers['subnet'] + ".#{i_lb + 20}"
        loadbalancer.vm.hostname = "lb-#{i_lb}"
        loadbalancer.vm.provider "virtualbox" do |v|
          v.customize ["modifyvm", :id, "--memory", 1024]
          v.customize ["modifyvm", :id, "--cpus", 1]
        end
      end
    end
  end

  (1..servers['master']['number']).each do |i_master|
    config.vm.define "k8s-master-#{i_master}" do |master|
      master.vm.box = servers['image']
      master.vm.network "private_network", ip: servers['subnet'] + ".#{i_master + 10}"
      master.vm.hostname = "k8s-master-#{i_master}"
      master.vm.provider "virtualbox" do |v|
        v.customize ["modifyvm", :id, "--memory", servers['master']['mem']]
        v.customize ["modifyvm", :id, "--cpus", servers['master']['cpu']]
      end
    end
  end

  (1..servers['worker']['number']).each do |i_worker|
    config.vm.define "k8s-node-#{i_worker}" do |worker|
      worker.vm.box = servers['image']
      worker.vm.network "private_network", ip: servers['subnet'] + ".#{i_worker + 30}"
      worker.vm.hostname = "k8s-node-#{i_worker}"
      worker.vm.provider "virtualbox" do |v|
        v.customize ["modifyvm", :id, "--memory", servers['worker']['mem']]
        v.customize ["modifyvm", :id, "--cpus", servers['worker']['cpu']]
        unless File.exist?(".vagrant/machines/k8s-node-#{i_worker}/virtualbox/disk-1.vdi")
          v.customize ['createhd', '--filename', ".vagrant/machines/k8s-node-#{i_worker}/virtualbox/disk-1.vdi", '--size', servers['worker']['disk_size_gb'] * 1024]
        end
        v.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', ".vagrant/machines/k8s-node-#{i_worker}/virtualbox/disk-1.vdi"]
      end
    end
  end

end
